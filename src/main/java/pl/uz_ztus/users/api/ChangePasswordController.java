package pl.uz_ztus.users.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import pl.uz_ztus.users.domain.model.ChangeUserPassword;
import pl.uz_ztus.users.domain.service.UserService;

@UserController
public class ChangePasswordController {

    private final UserService userService;

    @Autowired
    public ChangePasswordController(final UserService userService) {
        this.userService = userService;
    }

    @PutMapping(value = "/password")
    @ApiOperation("Changes user's password")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Password changed"),
            @ApiResponse(code = 403, message = "Wrong password"),
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public void changePassword(@RequestBody final ChangeUserPassword changeUserPassword) {
        this.userService.changeUserPassword(changeUserPassword);
    }
}
