package pl.uz_ztus.users.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pl.uz_ztus.users.domain.model.UserData;
import pl.uz_ztus.users.domain.model.dao.User;
import pl.uz_ztus.users.domain.service.UserService;

import javax.validation.Valid;

@UserController
public class CreateUserController {

    private final UserService userService;

    @Autowired
    public CreateUserController(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiOperation("Creates new user")
    @ApiResponses({
            @ApiResponse(code = 201, message = "User created"),
            @ApiResponse(code = 409, message = "User with given username/email already exist"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public User createUser(@RequestBody @Valid final UserData userData) {
        return this.userService.create(userData);
    }
}
