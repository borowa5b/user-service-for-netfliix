package pl.uz_ztus.users.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.uz_ztus.users.domain.service.UserService;

@UserController
public class DeleteUserController {

    private final UserService userService;

    @Autowired
    public DeleteUserController(final UserService userFacade) {
        this.userService = userFacade;
    }

    @DeleteMapping("/delete/{userId}")
    @ApiOperation("Deletes user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "User deleted"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public void deleteUser(@PathVariable("userId") final String userId) {
        this.userService.delete(Long.decode(userId));
    }
}
