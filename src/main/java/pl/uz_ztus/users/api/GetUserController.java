package pl.uz_ztus.users.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.uz_ztus.users.domain.model.dao.User;
import pl.uz_ztus.users.domain.service.UserService;

@UserController
public class GetUserController {

    private final UserService userService;

    @Autowired
    public GetUserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping("{username}/{password}")
    @ApiOperation("Gets user by username and password for signing in purposes")
    @ApiResponses({
            @ApiResponse(code = 200, message = "User fetched"),
            @ApiResponse(code = 403, message = "User entered wrong password"),
            @ApiResponse(code = 404, message = "User not found"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public User getUser(@PathVariable("username") final String username,
                        @PathVariable("password") final String password) {
        return this.userService.getUser(username, password);
    }
}
