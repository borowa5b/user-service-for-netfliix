package pl.uz_ztus.users.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import pl.uz_ztus.users.domain.model.dao.User;
import pl.uz_ztus.users.domain.service.UserService;

import java.util.List;

@UserController
public class GetUsersController {

    private final UserService userService;

    @Autowired
    public GetUsersController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping()
    @ApiOperation("Gets all users")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Users fetched"),
            @ApiResponse(code = 500, message = "Service unavailable")
    })
    public List<User> getUsers() {
        return this.userService.getAll();
    }
}
