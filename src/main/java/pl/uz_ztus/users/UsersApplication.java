package pl.uz_ztus.users;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class UsersApplication {

    public static void main(final String[] args) {
        SpringApplication.run(UsersApplication.class, args);
    }
}
