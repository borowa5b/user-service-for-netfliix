package pl.uz_ztus.users.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserNameAlreadyExistsException extends RuntimeException {

    public UserNameAlreadyExistsException(final String username) {
        super(MessageFormat.format("User with username {0} already exists", username));
    }
}
