package pl.uz_ztus.users.domain.model.dao;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.uz_ztus.users.domain.model.UserData;

import javax.persistence.*;

@Data
@Entity
@Table(name = "users")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true, nullable = false)
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    @Column(unique = true, nullable = false)
    private String email;

    public User(final UserData userData) {
        this.username = userData.getUsername();
        this.firstName = userData.getFirstName();
        this.lastName = userData.getLastName();
        this.email = userData.getEmail();
    }
}
