package pl.uz_ztus.users.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ChangeUserPassword {

    @NotNull
    @NotBlank
    private final Long userId;
    @NotNull
    @NotBlank
    private final String oldPassword;
    @NotNull
    @NotBlank
    private final String newPassword;
}
