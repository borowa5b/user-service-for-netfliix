package pl.uz_ztus.users.domain.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UserData {
    @NotNull
    @NotBlank
    private final String username;
    @NotNull
    @NotBlank
    private final String password;
    @NotNull
    @NotBlank
    private final String firstName;
    @NotNull
    @NotBlank
    private final String lastName;
    @NotNull
    @NotBlank
    private final String email;
}
