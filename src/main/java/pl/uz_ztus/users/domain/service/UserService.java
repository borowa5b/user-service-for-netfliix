package pl.uz_ztus.users.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.uz_ztus.users.domain.exception.*;
import pl.uz_ztus.users.domain.model.ChangeUserPassword;
import pl.uz_ztus.users.domain.model.UserData;
import pl.uz_ztus.users.domain.model.dao.User;
import pl.uz_ztus.users.domain.repository.UserRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(final UserRepository userRepository, final PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public User create(final UserData userData) {
        final String email = userData.getEmail();
        if (emailExist(email)) {
            throw new UserEmailAlreadyExistsException(email);
        }
        final String username = userData.getUsername();
        if (usernameExist(username)) {
            throw new UserNameAlreadyExistsException(username);
        }
        final User user = new User(userData);
        user.setPassword(passwordEncoder.encode(userData.getPassword()));
        return this.userRepository.save(user);
    }

    public User getUser(final String username, final String password) {
        final User user = this.userRepository.findByUsername(username);
        if (Objects.isNull(user)) {
            throw new UsernameNotFoundException(username);
        }
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new InvalidPasswordException();
        }
        return user;
    }

    @Transactional
    public void changeUserPassword(final ChangeUserPassword changeUserPassword) {
        final Long userId = changeUserPassword.getUserId();
        final Optional<User> userOptional = this.userRepository.findById(userId);
        if(!userOptional.isPresent()) {
            throw new UserNotFoundException(userId);
        }
        final User user = userOptional.get();
        final String oldPassword = changeUserPassword.getOldPassword();
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw new InvalidPasswordException();
        }
        final String newPassword = passwordEncoder.encode(changeUserPassword.getNewPassword());
        this.userRepository.updatePassword(newPassword, user.getId());
    }

    public List<User> getAll() {
        return this.userRepository.findAll();
    }

    public void delete(final Long userId) {
        this.userRepository.deleteById(userId);
    }

    private boolean usernameExist(final String username) {
        final User user = this.userRepository.findByUsername(username);
        return Objects.nonNull(user);
    }

    private boolean emailExist(final String email) {
        final User user = this.userRepository.findByEmail(email);
        return Objects.nonNull(user);
    }
}
